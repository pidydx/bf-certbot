#!/bin/bash

set -e

if [ "$1" = 'init' ]; then
    exec cert_manager
fi

if [ "$1" = 'certbot' ]; then
    exec yacron
fi

exec "$@"
