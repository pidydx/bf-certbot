import yaml


class Config(object):
    def __init__(self, config_file):
        with open(config_file, 'r') as fd:
            config = yaml.safe_load(fd)

        self.dns_validator = config.get('dns_validator', {})
        self.cert_manager = config.get('cert_manager', {})
