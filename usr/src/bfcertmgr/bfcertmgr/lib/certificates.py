import os
import logging
import subprocess
import OpenSSL
from datetime import datetime
from datetime import timedelta
from bfcertmgr.lib.publish import publish_secret


class CertManager(object):
    def __init__(self, config):
        self.storage_path = config.get('storage_path')
        self.renewal_window = config.get('renewal_window')

        self.certificates = []
        for attributes in config.get('certs', []):
            cert = Certificate(attributes, self.storage_path, self.renewal_window)
            if cert.valid:
                self.certificates.append(cert)
            else:
                logging.info('Skipping invalid configuration for: %s', cert.requested_subject_name)

    def check_certificates(self):
        for certificate in self.certificates:
            logging.info('Cert Path: %s', certificate.cert_path)
            logging.info('Requested Subject Name: %s',
                         certificate.requested_subject_name)
            logging.info('Existing Subject Name: %s',
                         certificate.existing_subject_name)
            logging.info('Requested Names: %s',
                         ','.join(certificate.requested_names))
            logging.info('Existing Names: %s',
                         ','.join(certificate.existing_names))
            logging.info('Requested Expiration: %s',
                         certificate.requested_expiration)
            logging.info('Existing Expiration: %s',
                         certificate.existing_expiration)
            logging.info('Current: %s', certificate.current)

    def renew_certificates(self, dry_run=False):
        for certificate in self.certificates:
            if not certificate.current:
                self.renew_certificate(certificate, dry_run)

    @staticmethod
    def renew_certificate(certificate, dry_run=False):
        logging.info('Renewing certificate: %s',
                     certificate.requested_subject_name)
        command = [
            'certbot certonly', '--no-eff-email', '-m',
            certificate.requesting_email
        ]

        if certificate.existing_cert:
            command.extend(['--cert-name', certificate.requested_subject_name])

        command.extend(['-d', certificate.requested_subject_name])

        for alternate_name in certificate.requested_alternate_names:
            command.extend(['-d', f'"{alternate_name}"'])

        if dry_run:
            command.append('--dry-run')

        logging.info('Executing: %s', ' '.join(command))
        try:
            results = subprocess.run(' '.join(command),
                                     timeout=30,
                                     shell=True,
                                     capture_output=True,
                                     encoding='utf-8')
            for line in results.stdout.split('\n'):
                if line:
                    logging.info(line)
            for line in results.stderr.split('\n'):
                if line:
                    logging.error(line)
        except subprocess.TimeoutExpired as e:
            logging.error('Failed to register cert for: %s', certificate.requested_subject_name)
            logging.error(e.output)


    def publish_certificates(self):
        for certificate in self.certificates:
            publish_secret(certificate)

class Certificate(object):
    def __init__(self, attributes, base_path, renewal_window):
        self.base_path = base_path
        self.renewal_window = renewal_window
        self.requested_subject_name = attributes.get('subject_name')
        self.requested_alternate_names = attributes.get('alternate_names')
        if not self.requested_alternate_names:
            self.requested_alternate_names = []
        self.requesting_email = attributes.get('email')

        try:
            with open(self.cert_path, 'r') as fd:
                self.existing_cert = OpenSSL.crypto.load_certificate(
                    OpenSSL.crypto.FILETYPE_PEM, fd.read())
        except:
            self.existing_cert = None
            logging.warn('Could not open cert at: %s', self.cert_path)

    @property
    def valid(self):
        valid = True
        if not isinstance(self.requested_subject_name, str):
            self.requested_subject_name = "Unknown"
            valid = False
        elif not isinstance(self.requested_alternate_names, list):
            valid = False
        elif not isinstance(self.requesting_email, str):
            valid = False
        return valid

    @property
    def existing_subject_name(self):
        name = ''
        if self.existing_cert:
            name = self.existing_cert.get_subject().get_components(
            )[0][1].decode('utf-8')
        return name

    @property
    def existing_names(self):
        names = []
        if self.existing_cert:
            for extension in range(0,
                                   self.existing_cert.get_extension_count()):
                if self.existing_cert.get_extension(
                        extension).get_short_name() == b'subjectAltName':
                    names = str(self.existing_cert.get_extension(6)).replace(
                        ' ', '').replace('DNS:', '').split(',')
        return names

    @property
    def existing_expiration(self):
        expiration = datetime.utcnow()
        if self.existing_cert:
            expiration = datetime.strptime(
                self.existing_cert.get_notAfter().decode('utf-8'),
                '%Y%m%d%H%M%SZ')
        return expiration

    @property
    def requested_names(self):
        return self.requested_alternate_names + [self.requested_subject_name]

    @property
    def requested_expiration(self):
        expiration = datetime.utcnow()
        if self.existing_cert:
            expiration = self.existing_expiration - timedelta(
                days=self.renewal_window)
        return expiration

    @property
    def key_path(self):
        return os.path.join(self.base_path, self.requested_subject_name,
                            'privkey.pem')

    @property
    def cert_path(self):
        return os.path.join(self.base_path, self.requested_subject_name,
                            'cert.pem')

    @property
    def fullchain_path(self):
        return os.path.join(self.base_path, self.requested_subject_name,
                            'fullchain.pem')

    @property
    def current(self):
        current = False
        if self.existing_cert:
            current = True
            if datetime.utcnow() > self.requested_expiration:
                logging.warn('Certificate for %s expires in renewal window.',
                             self.requested_subject_name)
                current = False

            if self.requested_subject_name != self.existing_subject_name:
                logging.warn(
                    'Certificate for %s does not match subject names.',
                    self.requested_subject_name)
                current = False

            if set(self.requested_names) != set(self.existing_names):
                logging.warn(
                    'Certificate for %s does not match subject alt names.',
                    self.requested_subject_name)
                current = False
        return current
