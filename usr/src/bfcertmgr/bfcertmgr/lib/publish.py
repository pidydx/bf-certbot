import logging
import sys
from base64 import b64encode
from datetime import datetime, timedelta
from cryptography import x509
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from kubernetes import client, config


def gen_self_signed(certificate):
    now = datetime.utcnow()

    key = rsa.generate_private_key(public_exponent=65537, key_size=2048)
    
    name = x509.Name([x509.NameAttribute(NameOID.COMMON_NAME, certificate.requested_subject_name)])
    san = x509.SubjectAlternativeName([x509.DNSName(name) for name in certificate.requested_names])
    basic_contraints = x509.BasicConstraints(ca=True, path_length=0)
    cert = (
        x509.CertificateBuilder()
        .subject_name(name)
        .issuer_name(name)
        .public_key(key.public_key())
        .serial_number(1000)
        .not_valid_before(now)
        .not_valid_after(now + timedelta(days=10*365))
        .add_extension(basic_contraints, False)
        .add_extension(san, False)
        .sign(key, hashes.SHA256())
    )

    return (key.private_bytes(encoding=serialization.Encoding.PEM, format=serialization.PrivateFormat.TraditionalOpenSSL, encryption_algorithm=serialization.NoEncryption()),
            cert.public_bytes(encoding=serialization.Encoding.PEM))

def publish_secret(certificate):
    try:
        with open(certificate.key_path, 'rb') as fd:
            key_data = fd.read()
        with open(certificate.fullchain_path, 'rb') as fd:
            cert_data = fd.read()
    except Exception:
        logging.warn('Could not load existing certificate, using self signed for: %s' % certificate.requested_subject_name)
        key_data, cert_data = gen_self_signed(certificate)

    config.load_incluster_config()
    v1 = client.CoreV1Api()

    with open('/var/run/secrets/kubernetes.io/serviceaccount/namespace', 'r') as fd:
        namespace = fd.read()

    secret = client.V1Secret(
        api_version='v1',
        data={'tls.crt': b64encode(cert_data).decode('utf-8'), 'tls.key': b64encode(key_data).decode('utf-8')},
        kind="Secret",
        metadata={'name': certificate.requested_subject_name, 'namespace': namespace},
        type = "kubernetes.io/tls"
    )

    logging.info('Publishing secret for %s' % certificate.requested_subject_name)
    try:
        v1.create_namespaced_secret(namespace, body=secret)
    except client.exceptions.ApiException as e:
        if e.status == 409:
            try:
                v1.replace_namespaced_secret(certificate.requested_subject_name, namespace, body=secret)
            except Exception:
                logging.error('Could not replace secret for %s' % certificate.requested_subject_name)
                sys.exit(1)
        else:
            logging.error('Could not create secret for %s' % certificate.requested_subject_name)
            sys.exit(1)
