import ldap
from ldap import modlist
import logging
from publicsuffix2 import PublicSuffixList
import sys


class ACMEDNSValidator(object):
    def __init__(self, config, certbot_domain, certbot_validation):
        self.ldap_user = config.get('ldap_user')
        self.ldap_pass = config.get('ldap_pass')
        self.ldap_basedn = config.get('ldap_basedn')
        self.ldap_url = config.get('ldap_url')

        psl = PublicSuffixList()
        zone_name = psl.get_public_suffix(certbot_domain)

        dns_zone_dn = f'dc={zone_name},ou=public,ou=dns,{self.ldap_basedn}'

        self.challenge_dn = f'dc=_acme-challenge,{dns_zone_dn}'

        self.challenge_entry = {
            'objectClass': [
                b'top', b'domain', b'dNSDomain', b'dnsdomain2',
                b'domainrelatedobject'
            ],
            'associatedDomain':
            ['.'.join(['_acme-challenge', certbot_domain]).encode('utf-8')],
            'tXTRecord': [certbot_validation.encode('utf-8')],
            'dc':
            ['_acme-challenge'.encode('utf-8')]
        }

    def _get_ldap_conn(self):
        conn = ldap.initialize(self.ldap_url)
        conn.bind_s(self.ldap_user, self.ldap_pass, ldap.AUTH_SIMPLE)
        return conn

    def _check_ldap_acme_challenge_entry(self):
        try:
            conn = self._get_ldap_conn()
            conn.search_s(self.challenge_dn, 0)
            conn.unbind_s()
            return True
        except ldap.NO_SUCH_OBJECT:
            return False

    def _add_ldap_acme_challenge_entry(self):
        conn = self._get_ldap_conn()
        conn.add_s(self.challenge_dn,
                   ldap.modlist.addModlist(self.challenge_entry))
        conn.unbind_s()

    def _del_ldap_acme_challenge_entry(self):
        conn = self._get_ldap_conn()
        conn.delete_s(self.challenge_dn)
        conn.unbind_s()

    def setup(self):
        try:
            if self._check_ldap_acme_challenge_entry():
                self._del_ldap_acme_challenge_entry()

            self._add_ldap_acme_challenge_entry()
            logging.info('Created entry: %s', self.challenge_dn)
        except ldap.LDAPError as e:
            logging.error('Unable to add entry %s: %s', self.challenge_dn, e)
            sys.exit(1)

    def cleanup(self):
        try:
            self._del_ldap_acme_challenge_entry()
            logging.info('Deleted entry: %s', self.challenge_dn)
        except ldap.NO_SUCH_OBJECT:
            logging.error('No entry to delete: %s', self.challenge_dn)
        except ldap.LDAPError as e:
            logging.error('Unable to delete entry %s: %s', self.challenge_dn, e)
            sys.exit(1)
