#!/usr/bin/python3

import argparse
import logging
from bfcertmgr.lib.config import Config
from bfcertmgr.lib.certificates import CertManager


def main():
    logging.basicConfig(level='INFO')

    parser = argparse.ArgumentParser()
    parser.add_argument('--config',
                        default='/etc/bfcertmgr/config.yaml',
                        help='Path to config file')
    parser.add_argument('--dry-run',
                        action='store_true',
                        help='Do not renew any certificates.')

    args = parser.parse_args()

    config = Config(args.config)
    cert_manager = CertManager(config.cert_manager)
    cert_manager.check_certificates()
    cert_manager.renew_certificates(args.dry_run)
    if config.cert_manager.get('publish'):
        cert_manager.publish_certificates()

if __name__ == '__main__':
    main()
