#!/usr/bin/python3

import argparse
import logging
import os
import sys
from bfcertmgr.lib.validators import ACMEDNSValidator
from bfcertmgr.lib.config import Config


def main():
    logging.basicConfig(level='INFO')

    parser = argparse.ArgumentParser()
    parser.add_argument('--config',
                        default='/etc/bfcertmgr/config.yaml',
                        help='Path to config file')
    parser.add_argument('--cleanup', action='store_true', help='Remove entry')
    parser.add_argument('--certbot-validation',
                        default=os.environ.get('CERTBOT_VALIDATION', ''),
                        help='Override CERTBOT_VALIDATION env var')
    parser.add_argument('--certbot-domain',
                        default=os.environ.get('CERTBOT_DOMAIN', ''),
                        help='Override CERTBOT_DOMAIN env var')
    args = parser.parse_args()

    config = Config(args.config)

    if args.certbot_validation != '' and args.certbot_domain != '':
        acme_validator = ACMEDNSValidator(config.dns_validator,
                                          args.certbot_domain,
                                          args.certbot_validation)
    else:
        logging.error(
            'Certbot Domain and Certbot Validation must be provided.')
        sys.exit(1)

    if args.cleanup:
        acme_validator.cleanup()
    else:
        acme_validator.setup()


if __name__ == '__main__':
    main()
