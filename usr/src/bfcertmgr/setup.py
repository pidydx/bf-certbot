# -*- coding: utf-8 -*-

import os
import glob

from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE.md') as f:
    license = f.read()

with open('requirements.txt') as fd:
    requires = fd.readlines()

setup(
    name='bfcertmgr',
    description='Blackflag Cert Manager',
    long_description=readme,
    author='Blackflag',
    author_email='pidydx@blackflag.digital',
    url='na',
    license=license,
    packages=find_packages(),
    install_requires=requires,
    entry_points={
        'console_scripts': [
            'dns_validator = bfcertmgr.scripts.dns_validator:main',
            'cert_manager = bfcertmgr.scripts.cert_manager:main'
        ]
    }
)

