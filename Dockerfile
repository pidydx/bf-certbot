#########################
# Create base container #
#########################
FROM ubuntu:24.04 as base
LABEL maintainer="pidydx"

# Set app user and group
ENV APP_USER=certbot
ENV APP_GROUP=certbot
ENV VENV_PATH=/usr/local/share/bfcertmgr

# Set base dependencies
ENV BASE_DEPS ca-certificates libldap2 python3

# Set build dependencies
ENV BUILD_DEPS build-essential git python3-venv python3-dev libldap2-dev libsasl2-dev libffi-dev

# Create app user and group
RUN userdel ubuntu
RUN groupadd -g 1000 ${APP_GROUP} \
 && useradd -M -N -u 1000 ${APP_USER} -g ${APP_GROUP} -d /var/lib/${APP_USER} -s /usr/sbin/nologin

# Update and install base dependencies
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_DEPS} \
 && rm -rf /var/lib/apt/lists/*


##########################
# Create build container #
##########################
FROM base AS builder

# Install build dependencies
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BUILD_DEPS}

# Run build
COPY usr/src /usr/src
WORKDIR /usr/src
RUN python3 -m venv ${VENV_PATH} --copies \
 && ${VENV_PATH}/bin/pip3 install --upgrade pip wheel setuptools \
 && ${VENV_PATH}/bin/pip3 install ./bfcertmgr

##########################
# Create final container #
##########################
FROM base

# Finalize install
COPY etc/ /etc/
COPY --from=builder /usr/local /usr/local/
COPY usr/local /usr/local

RUN update-ca-certificates

# Prepare container
ENV PATH=${PATH}:${VENV_PATH}/bin
VOLUME ["/etc/letsencrypt", "/etc/bfcertmgr"]
USER $APP_USER

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["certbot"]
